package ca.nait.dmit.controller;

import javax.enterprise.context.RequestScoped; 
import javax.inject.Named;
import ca.nait.dmit.domain.BMI; 
import helper.JSFHelper;

@Named
@RequestScoped
public class BmiController {
	
	private int weight;
	private int height;

	public int getWeight()
	{
		return weight;
	}
	
	public void setWeight(int weight)
	{
		this.weight = weight; 
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public void setHeight(int height)
	{
		this.height = height; 
	}
	
	public void calculate()
	{
		BMI currentBMI = new BMI(weight, height);
		String message = String.format("Your BMI is %.1f, you are %s", currentBMI.bmi(), currentBMI.bmiCategory()); 
		JSFHelper.addInfoMessage(message); 
	}

}
