package ca.nait.dmit.controller;

import java.util.Calendar;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import helper.JSFHelper;

@Named
@RequestScoped
public class ChineseZodiacController {
	private int birthYear = Calendar.getInstance().get(Calendar.YEAR); // by default, the current year will be displayed in the field
	private String animalImageUrl = "/resources/images/chinese_zodiac.jpg"; // the URL of the image that will change according to the year
	
	/**
	 * This method will calculate the given year, change the image and display a message
	 */
	public void calculate() {
		// the year must be at least 1900, or else a warning message will be displayed and the method will stop
		if(birthYear < 1900) {
			String message = "The birth year must be after 1899.";
			JSFHelper.addWarningMessage(message);
			return;
		}
		
		String animalSign = ""; // string to hold the animal name
		int offset = (birthYear - 1900) % 12; // the formula to calculate the corresponding animal
		
		// assigns the animal based on the calculated value
		switch(offset) {
		case 0:
			animalSign = "Rat";		
			animalImageUrl = "/resources/images/Rat.jpg";
			break;
		case 1:
			animalSign = "Ox";
			animalImageUrl = "/resources/images/Ox.jpg";
			break;
		case 2:
			animalSign = "Tiger";
			animalImageUrl = "/resources/images/Tiger.jpg";
			break;
		case 3:
			animalSign = "Rabbit";
			animalImageUrl = "/resources/images/Rabbit.jpg";
			break;
		case 4:
			animalSign = "Dragon";
			animalImageUrl = "/resources/images/Dragon.jpg";
			break;
		case 5:
			animalSign = "Snake";
			animalImageUrl = "/resources/images/Snake.jpg";
			break;
		case 6:
			animalSign = "Horse";
			animalImageUrl = "/resources/images/Horse.jpg";
			break;
		case 7:
			animalSign = "Goat";
			animalImageUrl = "/resources/images/Goat.jpg";
			break;
		case 8:
			animalSign = "Monkey";
			animalImageUrl = "/resources/images/Monkey.jpg";
			break;
		case 9:
			animalSign = "Rooster";
			animalImageUrl = "/resources/images/Rooster.jpg";
			break;
		case 10:
			animalSign = "Dog";
			animalImageUrl = "/resources/images/Dog.jpg";
			break;
		case 11:
			animalSign = "Pig";
			animalImageUrl = "/resources/images/Pig.jpg";
			break;
		default:
			break;
		}
		
		// displays a message to the user regarding the animal
		String message = birthYear + " is the year of the " + animalSign;
		JSFHelper.addInfoMessage(message);
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}

	public String getAnimalImageUrl() {
		return animalImageUrl;
	}

	public void setAnimalImageUrl(String animalImageUrl) {
		this.animalImageUrl = animalImageUrl;
	}
}