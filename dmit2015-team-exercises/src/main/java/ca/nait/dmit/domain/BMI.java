package ca.nait.dmit.domain;

public class BMI {
	
	private int weight;
	private int height;
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public BMI(int weight, int height) 
	{
		super();
		this.weight = weight;
		this.height = height;
	}
	public BMI() {
		super();
		// TODO Auto-generated constructor stub		
	}
	
	public double bmi()
	{		
		return (703 * weight) / Math.pow(height, 2);		
	}
	
	public String bmiCategory()
	{
		String Classification = "error";
		double bmi = bmi();
		if(bmi < 18.5)
		{
			Classification = "Underweight";
		}
		else if (bmi >= 18.50 && bmi <= 24.99)
		{
			Classification = "Normal Weight";
		}
		else if (bmi >= 25.00 && bmi <= 29.99)
		{
			Classification = "Overweight";
		}
		else if (bmi >= 30.00 && bmi <= 34.99)
		{
			Classification = "Obese class 1";
		}
		else if (bmi >= 35.00 && bmi <= 39.99)
		{
			Classification = "Obese Class 2";
		}
		else if (bmi >= 40.00)
		{
			Classification = "Obese Class 3";
		}
		
		return Classification;
		
		
	}
	
	public String HealthRisk()
	{
		String Risk = "";
		double bmi = bmi();
		if(bmi < 18.5)
		{
			Risk = "Increased";
		}
		else if (bmi >= 18.50 && bmi <= 24.99)
		{
			Risk = "Least";
		}
		else if (bmi >= 25.00 && bmi <= 29.99)
		{
			Risk = "Increased";
		}
		else if (bmi >= 30.00 && bmi <= 34.99)
		{
			Risk = "High";
		}
		else if (bmi >= 35.00 && bmi <= 39.99)
		{
			Risk = "Very high";
		}
		else if (bmi >= 40.00)
		{
			Risk = "Extremely High";
		}
		
		return Risk;
	}

}
