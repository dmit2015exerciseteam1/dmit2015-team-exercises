package ca.nait.dmit.domain;

public class Loan {
	
	double mortgageAmount;
	double annualInterestRate;
	int amortizationPeriod;
	
	public Loan(double mortgageAmount, double annualInterestRate, int amortizationPeriod) {
		super();
		this.mortgageAmount = mortgageAmount;
		this.annualInterestRate = annualInterestRate;
		this.amortizationPeriod = amortizationPeriod;
	}
	
	/**
	 * Rounds a double value to 2 decimal places
	 * @param valueToRound the value to round
	 * @return the value rounded to 2 decimal places
	 */
	 public static double roundTo2Decimals(double valueToRound)
	 {
		 return Math.round( valueToRound * 100.0 ) / 100.0;
	 }
	
	public double monthlyPayment()
	{
		
		double monthlyPayment;
		
		monthlyPayment = (mortgageAmount * (Math.pow((1.0+(annualInterestRate/200.0)), (1.0/6.0))-1.0)/(1.0-(Math.pow(Math.pow(1.0+(annualInterestRate/200.0),(1.0/6.0)), (-12.0*amortizationPeriod))))); 
		
		return roundTo2Decimals(monthlyPayment);
		
	}
	
	public LoanSchedule[] loanScheduleTable()
	{
	// number of payments is the loan term (in years) multiply by the number of months per year (12)
	int numberOfPayments = amortizationPeriod * 12;
	LoanSchedule[] loanScheduleArray = new LoanSchedule[ numberOfPayments ];
	// set the initial remaining balance is equal to amount borrowed
	double remainingBalance = mortgageAmount;
	// calculate monthlyPercentageRate
	double monthlyPercentageRate = (Math.pow(1.0+(annualInterestRate/200.0),(1.0/6.0))-1.0);
	for( int paymentNumber = 1; paymentNumber <= numberOfPayments; paymentNumber++)
	{
	// calculate interestPaid and round to 2 decimal places
		double interestPaid = roundTo2Decimals(monthlyPercentageRate * remainingBalance);
	// calculate principalPaid and round to 2 decimal places
		double principalPaid = roundTo2Decimals(monthlyPayment() - interestPaid);
	// update remainingBalance and round to 2 decimal places
	// remainingBalance = roundTo2Decimals(remainingBalance - principalPaid);
	// set remainingBalance to zero if it is calculated to be less than zero
		if(remainingBalance <= monthlyPayment())
		{
			principalPaid = remainingBalance;
			remainingBalance = 0;			
		}
		else
		{
			remainingBalance = roundTo2Decimals(remainingBalance - principalPaid);
		}
	// arrays in Java are 0-index based
	loanScheduleArray[ paymentNumber - 1 ] = new LoanSchedule( paymentNumber, interestPaid, principalPaid, remainingBalance );
	}
	return loanScheduleArray;
	}
}
