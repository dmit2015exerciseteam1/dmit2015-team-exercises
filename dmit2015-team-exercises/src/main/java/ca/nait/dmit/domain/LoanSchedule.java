package ca.nait.dmit.domain;

public class LoanSchedule {
	
	private int paymentNumber;
	private double interestPaid;
	private double PrincipalPaid;
	private double remainingBalance;
	
	

	public int getPaymentNumber() {
		return paymentNumber;
	}



	public void setPaymentNumber(int paymentNumber) {
		this.paymentNumber = paymentNumber;
	}



	public double getInterestPaid() {
		return interestPaid;
	}



	public void setInterestPaid(double interestPaid) {
		this.interestPaid = interestPaid;
	}



	public double getPrincipalPaid() {
		return PrincipalPaid;
	}



	public void setprincipalPaid(double PrincipalPaid) {
		this.PrincipalPaid = PrincipalPaid;
	}



	public double getRemainingBalance() {
		return remainingBalance;
	}



	public void setRemainingBalance(double remainingBalance) {
		this.remainingBalance = remainingBalance;
	}



	public LoanSchedule() {
		super();
		// TODO Auto-generated constructor stub
	}



	public LoanSchedule(int paymentNumber, double interestPaid, double PrincipalPaid, double remainingBalance) {
		super();
		this.paymentNumber = paymentNumber;
		this.interestPaid = interestPaid;
		this.PrincipalPaid = PrincipalPaid;
		this.remainingBalance = remainingBalance;
	}
	
}
